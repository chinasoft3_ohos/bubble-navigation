/*
        Copyright 2019 Gaurav Kumar

        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
        http://www.apache.org/licenses/LICENSE-2.0
        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.
*/

package com.gauravk.bubblenavigation.listener;

/**
 * IBubbleNavigation
 *
 * @since 2021-04-09
 */
@SuppressWarnings("unused")
public interface IBubbleNavigation {
    /**
     * 设置导航item改变监听回调
     *
     * @param navigationChangeListener 接口回调
     */
    void setNavigationChangeListener(BubbleNavigationChangeListener navigationChangeListener);

    /**
     * 获取当前被选中的item的position
     *
     * @return position
     */
    int getCurrentActiveItemPosition();

    /**
     * 设置当前被选中的item
     *
     * @param position position
     */
    void setCurrentActiveItem(int position);

    /**
     * 设置角标的显示文字
     *
     * @param position position
     * @param value    显示文字
     */
    void setBadgeValue(int position, String value);
}
