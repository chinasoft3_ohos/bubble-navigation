/*
        Copyright 2019 Gaurav Kumar

        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
        http://www.apache.org/licenses/LICENSE-2.0
        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.
*/
package com.gauravk.bubblenavigation;

import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

/**
 * BubbleToggleItem
 *
 * @since 2021-04-09
 */
public class BubbleToggleItem {

    private Element icon;
    private Element iconSel;
    private Element shape;
    private Element shapeRect;
    private String title = "";

    private Color colorActive;
    private Color colorInactive;

    private String badgeText;
    private Color badgeTextColor;
    private Color badgeBackgroundColor;

    private int titleSize;
    private int badgeTextSize;
    private int iconWidth;
    private int iconHeight;

    private int titlePadding;
    private int internalPadding;

    BubbleToggleItem() {
    }

    Element getIcon() {
        return icon;
    }

    void setIcon(Element icon) {
        this.icon = icon;
    }

    Element getShape() {
        return shape;
    }

    void setShape(Element shape) {
        this.shape = shape;
    }

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    Color getColorActive() {
        return colorActive;
    }

    void setColorActive(Color colorActive) {
        this.colorActive = colorActive;
    }

    Color getColorInactive() {
        return colorInactive;
    }

    void setColorInactive(Color colorInactive) {
        this.colorInactive = colorInactive;
    }

    int getTitleSize() {
        return titleSize;
    }

    void setTitleSize(int titleSize) {
        this.titleSize = titleSize;
    }

    int getIconWidth() {
        return iconWidth;
    }

    void setIconWidth(int iconWidth) {
        this.iconWidth = iconWidth;
    }

    int getIconHeight() {
        return iconHeight;
    }

    void setIconHeight(int iconHeight) {
        this.iconHeight = iconHeight;
    }

    int getTitlePadding() {
        return titlePadding;
    }

    void setTitlePadding(int titlePadding) {
        this.titlePadding = titlePadding;
    }

    int getInternalPadding() {
        return internalPadding;
    }

    void setInternalPadding(int internalPadding) {
        this.internalPadding = internalPadding;
    }

    Color getBadgeTextColor() {
        return badgeTextColor;
    }

    void setBadgeTextColor(Color badgeTextColor) {
        this.badgeTextColor = badgeTextColor;
    }

    Color getBadgeBackgroundColor() {
        return badgeBackgroundColor;
    }

    void setBadgeBackgroundColor(Color badgeBackgroundColor) {
        this.badgeBackgroundColor = badgeBackgroundColor;
    }

    int getBadgeTextSize() {
        return badgeTextSize;
    }

    void setBadgeTextSize(int badgeTextSize) {
        this.badgeTextSize = badgeTextSize;
    }

    String getBadgeText() {
        return badgeText;
    }

    void setBadgeText(String badgeText) {
        this.badgeText = badgeText;
    }

    Element getShapeRect() {
        return shapeRect;
    }

    void setShapeRect(Element shapeRect) {
        this.shapeRect = shapeRect;
    }

    Element getIconSel() {
        return iconSel;
    }

    void setIconSel(Element iconSel) {
        this.iconSel = iconSel;
    }
}
