/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gauravk.bubblebarsample.slice;

import com.gauravk.bubblebarsample.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * MainAbilitySlice
 *
 * @since 2021-04-09
 */
public class MainAbilitySlice extends AbilitySlice {
    // 两次点击按钮之间的点击间隔不能少于1000毫秒
    private static final int MIN_CLICK_DELAY_TIME = 800;
    private static long lastClickTime;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_open_top_navigation_bar).setClickedListener(component -> {
            if(isFastClick()){
                launchTopBarActivity();
            }
        });

        findComponentById(ResourceTable.Id_open_top_float_navigation_bar).setClickedListener(component -> {
            if(isFastClick()){
                launchFloatingBarActivity();
            }
        });

        findComponentById(ResourceTable.Id_open_bottom_equal_navigation_bar).setClickedListener(component -> {
            if(isFastClick()){
                launchEqualBarActivity();
            }
        });

        findComponentById(ResourceTable.Id_open_bottom_navigation_bar).setClickedListener(component -> {
            if(isFastClick()){
                launchBottomBarActivity();
            }
        });
    }

    private void launchBottomBarActivity() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withDeviceId("")
                .withAbilityName(BottomBarAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    private void launchFloatingBarActivity() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withDeviceId("")
                .withAbilityName(FloatingTopBarAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    private void launchTopBarActivity() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withDeviceId("")
                .withAbilityName(TopBarAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    private void launchEqualBarActivity() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withDeviceId("")
                .withAbilityName(EqualBottomBarAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    /**
     * 防重复点击
     *
     * @return 是否重复点击
     */
    private boolean isFastClick() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }
}
