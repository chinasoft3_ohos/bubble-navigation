/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gauravk.bubblebarsample.slice;

import com.gauravk.bubblebarsample.ResourceTable;
import com.gauravk.bubblebarsample.adapters.ScreenSlidePagerProvider;
import com.gauravk.bubblenavigation.BubbleNavigationLinearView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;

import java.util.ArrayList;

/**
 * TopBarAbilitySlice
 *
 * @since 2021-04-09
 */
public class TopBarAbilitySlice extends AbilitySlice {
    private BubbleNavigationLinearView mNavigationConstraintView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_top_bar);

        ArrayList<DependentLayout> fragList = new ArrayList<>();

        Text slideTitle;
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView = (DependentLayout) layoutScatter.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout = (DependentLayout) inflatedView.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(255, 183, 77));
        nodeLayout.setBackground(shapeElement);
        slideTitle = (Text) inflatedView.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle.setText(getString(ResourceTable.String_restaurant));
        fragList.add(inflatedView);

        Text slideTitle1;
        LayoutScatter layoutScatter1 = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView1 = (DependentLayout) layoutScatter1.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout1 = (DependentLayout) inflatedView1.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setRgbColor(new RgbColor(229, 115, 115));
        nodeLayout1.setBackground(shapeElement1);
        slideTitle1 = (Text) inflatedView1.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle1.setText(getString(ResourceTable.String_room));
        fragList.add(inflatedView1);

        Text slideTitle2;
        LayoutScatter layoutScatter2 = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView2 = (DependentLayout) layoutScatter2.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout2 = (DependentLayout) inflatedView2.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement2 = new ShapeElement();
        shapeElement2.setRgbColor(new RgbColor(129, 199, 132));
        nodeLayout2.setBackground(shapeElement2);
        slideTitle2 = (Text) inflatedView2.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle2.setText(getString(ResourceTable.String_happy));
        fragList.add(inflatedView2);

        initPagerSlider(fragList);
    }

    /**
     * 初始化布局
     *
     * @param fragList 添加的item列表
     */
    private void initPagerSlider(ArrayList<DependentLayout> fragList) {
        mNavigationConstraintView =
                (BubbleNavigationLinearView) findComponentById(ResourceTable.Id_top_navigation_constraint);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_view_pager);

        ScreenSlidePagerProvider pagerAdapter = new ScreenSlidePagerProvider(this, fragList);
        pageSlider.setProvider(pagerAdapter);
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int i) {
            }

            @Override
            public void onPageChosen(int i) {
                mNavigationConstraintView.setCurrentActiveItem(i);
            }
        });

        mNavigationConstraintView.setNavigationChangeListener((view, position) ->
                pageSlider.setCurrentPage(position, true));
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if (mNavigationConstraintView.mEventHandler != null){
            mNavigationConstraintView.mEventHandler.removeAllEvent();
            mNavigationConstraintView.mEventHandler = null;
        }
    }
}
