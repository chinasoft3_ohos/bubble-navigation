/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gauravk.bubblebarsample.slice;

import com.gauravk.bubblebarsample.ResourceTable;
import com.gauravk.bubblebarsample.adapters.ScreenSlidePagerProvider;
import com.gauravk.bubblenavigation.BubbleNavigationLinearView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;

import java.util.ArrayList;

/**
 * BottomBarAbilitySlice
 *
 * @since 2021-04-09
 */
public class BottomBarAbilitySlice extends AbilitySlice {
    private BubbleNavigationLinearView mBubbleNavigationLinearView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_bottom_bar);

        ArrayList<DependentLayout> fragList = new ArrayList<>();

        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView = (DependentLayout) layoutScatter.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout = (DependentLayout) inflatedView.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(229, 115, 115));
        nodeLayout.setBackground(shapeElement);
        Text slideTitle = (Text) inflatedView.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle.setText(getString(ResourceTable.String_home));
        fragList.add(inflatedView);

        LayoutScatter layoutScatter1 = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView1 = (DependentLayout) layoutScatter1.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout1 = (DependentLayout) inflatedView1.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setRgbColor(new RgbColor(100, 181, 246));
        nodeLayout1.setBackground(shapeElement1);
        Text slideTitle1 = (Text) inflatedView1.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle1.setText(getString(ResourceTable.String_search));
        fragList.add(inflatedView1);

        LayoutScatter layoutScatter2 = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView2 = (DependentLayout) layoutScatter2.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout2 = (DependentLayout) inflatedView2.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement2 = new ShapeElement();
        shapeElement2.setRgbColor(new RgbColor(144, 164, 174));
        nodeLayout2.setBackground(shapeElement2);
        Text slideTitle2 = (Text) inflatedView2.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle2.setText(getString(ResourceTable.String_likes));
        fragList.add(inflatedView2);

        LayoutScatter layoutScatter3 = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView3 = (DependentLayout) layoutScatter3.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout3 = (DependentLayout) inflatedView3.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement3 = new ShapeElement();
        shapeElement3.setRgbColor(new RgbColor(129, 199, 132));
        nodeLayout3.setBackground(shapeElement3);
        Text slideTitle3 = (Text) inflatedView3.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle3.setText(getString(ResourceTable.String_notification));
        fragList.add(inflatedView3);

        LayoutScatter layoutScatter4 = LayoutScatter.getInstance(getContext());
        DependentLayout inflatedView4 = (DependentLayout) layoutScatter4.parse(
                ResourceTable.Layout_ability_screen_slide_page, null, false);
        DependentLayout nodeLayout4 = (DependentLayout) inflatedView4.findComponentById(ResourceTable.Id_node_layout);
        ShapeElement shapeElement4 = new ShapeElement();
        shapeElement4.setRgbColor(new RgbColor(186, 104, 200));
        nodeLayout4.setBackground(shapeElement4);
        Text slideTitle4 = (Text) inflatedView4.findComponentById(ResourceTable.Id_screen_slide_title);
        slideTitle4.setText(getString(ResourceTable.String_profile));
        fragList.add(inflatedView4);

        initPagerSlider(fragList);
    }

    /**
     * 初始化布局
     *
     * @param fragList 添加的item列表
     */
    private void initPagerSlider(ArrayList<DependentLayout> fragList) {
        ScreenSlidePagerProvider pagerAdapter = new ScreenSlidePagerProvider(this, fragList);

        mBubbleNavigationLinearView =
                (BubbleNavigationLinearView) findComponentById(ResourceTable.Id_bottom_navigation_view_linear);

        mBubbleNavigationLinearView.setBadgeValue(0, "40");
        mBubbleNavigationLinearView.setBadgeValue(1, null);
        mBubbleNavigationLinearView.setBadgeValue(2, "7");
        mBubbleNavigationLinearView.setBadgeValue(3, "2");
        mBubbleNavigationLinearView.setBadgeValue(4, " ");

        final PageSlider viewPager = (PageSlider) findComponentById(ResourceTable.Id_view_pager);
        viewPager.setProvider(pagerAdapter);
        viewPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int i) {
            }

            @Override
            public void onPageChosen(int i) {
                mBubbleNavigationLinearView.setCurrentActiveItem(i);
            }
        });

        mBubbleNavigationLinearView.setNavigationChangeListener((view, position) ->
                viewPager.setCurrentPage(position, true));
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if (mBubbleNavigationLinearView.mEventHandler != null){
            mBubbleNavigationLinearView.mEventHandler.removeAllEvent();
            mBubbleNavigationLinearView.mEventHandler = null;
        }
    }
}
