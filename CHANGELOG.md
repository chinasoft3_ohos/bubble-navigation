## 1.0.0
* 正式版发布

## 0.0.2-SNAPSHOT
* ohos 第二个版本

1. 修改部分代码bug
2. 适配最新的sdk

## 0.0.1-SNAPSHOT
* ohos 第一个版本

实现了原库的全部 api，部分效果未实现
1. 点击TOP NAVIGATION BAR、FLOAT TOP NAVIGATION BAR页面的各个标签的空白处，原项目不可点击，openharmony可以点击，  
   该效果是因为原项目是基于ConstraintLayout来实现的，但是openharmony没有与之对应的组件
2. 点击按钮无水波纹效果，该效果是原项目系统自带特性，openharmony无该特性
