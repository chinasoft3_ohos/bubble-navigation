# bubble-navigation


## 项目介绍
项目名称：bubble-navigation

所属系列：openharmony的第三方组件适配移植

功能：轻量级的自定义导航栏组件

基线版本：Releases 1.0.4

项目移植状态：主功能完成

调用差异：无

开发版本：sdk6，DevEco Studio 2.2 Beta1

## 效果演示
![输入图片说明](https://gitee.com/chinasoft3_ohos/bubble-navigation/raw/master/img/4.gif  "4.gif")

## 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:bubblenavigation:1.0.0')
    ......  
 }
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行，如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
```
    // xml code
    <com.gauravk.bubblenavigation.BubbleNavigationLinearView
        ohos:id="$+id:floating_top_bar_navigation"
        ohos:height="80vp"
        ohos:width="match_parent"
        ohos:alignment="horizontal_center"
        ohos:background_element="$graphic:rounded_rect_white"
        ohos:left_margin="18vp"
        ohos:orientation="horizontal"
        ohos:padding="16vp"
        ohos:right_margin="18vp"
        ohos:top_margin="18vp">

        <com.gauravk.bubblenavigation.BubbleToggleView
            ohos:id="$+id:l_item_home"
            ohos:height="match_content"
            ohos:width="match_content"
            app:bt_active="true"
            app:bt_colorActive="$color:red_active"
            app:bt_colorInactive="$color:red_inactive"
            app:bt_icon="$graphic:ic_home"
            app:bt_sel_icon="$graphic:ic_sel_home"
            app:bt_shape="$graphic:white_background_oval"
            app:bt_shapeRect="$graphic:shape_home"
            app:bt_title="$string:home"/>

        <!-- Add more child items here - max upto 5 -->

    </com.gauravk.bubblenavigation.BubbleNavigationLinearView>
    
    // java code
    bubbleNavigation.setNavigationChangeListener(new BubbleNavigationChangeListener() {
        @Override
        public void onNavigationChanged(View view, int position) {
            //navigation changed, do something
        }
    });  
```

| **attr**     | **Description**  |
| -------------| ------------- |
| bt_active    | Sets to Active State |
| bt_colorActive     | When in Active State, uses this color for the Icon and Title  |
| bt_colorInctive     | When in Inactive State, uses this color for the Icon and Title  |
| bt_icon     | Sets the Icon Drawable |
| bt_iconWidth     | Updates the Icon Width|
| bt_iconHeight    | Updates the Icon Height |
| bt_title     | Sets the Title Text |
| bt_titleSize | Updates the Tilte Text Size in integer |
| bt_shape     | Sets the Background Element |
| bt_showShapeAlways  |  If true and using Normal element, background shape remains visible always |
| bt_shapeColor  |  Changes the tint color of the shape |
| bt_duration     |  Sets time duration for toggle animation to complete in ms |
| bt_padding     |  Sets the internal padding in integer |
| bt_titlePadding | Sets the title padding in integer |
| bt_badgeText | Sets the text for the badge |
| bt_badgeTextSize | Sets the font size of the badge text |
| bt_badgeTextColor | Sets the text color of the badge |
| bt_badgeBackgroundColor | Sets the background color of the badge |
| bt_modeDisplay     | Changes the display mode the child elements     |
|              | - **spread** : equally distributes the child elements |
|              | - **packed** : elements are packed with center gravity|
|              | - **inside** : inside elements are equally distributed|


## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
- 1.0.0

## 版权和许可信息
```
    Copyright 2019 Gaurav Kumar

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 ```